cp is the command for copy. Here are some of the ways that it can be used:

cp file1 file2          This copies the contents of file1 into file2 (overwriting what was originally in file2).
                        If there is no file2, then it will be created.
                        
cp file1 dir1           This makes a copy of file1 and puts it in directory1.

cp -r dir1 dir2         This copies directory1 and its contents and puts a copy inside directory2. If directory2 
                        doesn't already exist then it gets created.
